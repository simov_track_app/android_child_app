package simov.isep.pt.simovchildapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import simov.isep.pt.simovchildapp.rest.Tracking;

/**
 * Created by joao on 28/11/2017.
 */

public class TrackingAdapter extends ArrayAdapter<Tracking> {
    List<Tracking> trackingList;

    public TrackingAdapter(@NonNull Context context, int resource, @NonNull List<Tracking> objects) {
        super(context, resource, objects);
        trackingList = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        // first check to see if the view is null. if so, we have to inflate it.
        // to inflate it basically means to render, or show, the view.
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.tracking_row, null);
        }

		/*
         * Recall that the variable position is sent in as an argument to this method.
		 * The variable simply refers to the position of the current object in the list. (The ArrayAdapter
		 * iterates through the list we sent it)
		 *
		 * Therefore, i refers to the current Item object.
		 */
        Tracking tracking = trackingList.get(position);
        TextView parentUsername = v.findViewById(R.id.parent_username);
        TextView coordinates = v.findViewById(R.id.coordinates);
        TextView range = v.findViewById(R.id.range);
        TextView trackingId = v.findViewById(R.id.tracking_id);

        trackingId.setText("Tracking ID " + tracking.getId());
        parentUsername.setText(tracking.getParentUsername());
        if (tracking.getAllowedZone().getLocationLatitude().equals(0.0d)) {
            coordinates.setText("Coordinates Not defined");
            range.setText("Range Not defined");
        } else {
            coordinates.setText(tracking.getAllowedZone().getLocationLatitude() + "," +
                    tracking.getAllowedZone().getLocationLongitude());
            range.setText("Range (m): " + tracking.getAllowedZone().getRadiusMeters());
        }

        return v;
    }
}
