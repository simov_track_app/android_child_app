package simov.isep.pt.simovchildapp.rest;

/**
 * Created by joao on 28/11/2017.
 */

public class AllowedZone {
    Double locationLongitude;
    Double locationLatitude;
    Integer radiusMeters;

    public AllowedZone(Double locationLongitude, Double locationLatitude, Integer radiusMeters) {
        this.locationLongitude = locationLongitude;
        this.locationLatitude = locationLatitude;
        this.radiusMeters = radiusMeters;
    }

    public AllowedZone() {

    }

    public Double getLocationLongitude() {

        return locationLongitude;
    }

    public void setLocationLongitude(Double locationLongitude) {
        this.locationLongitude = locationLongitude;
    }

    public Double getLocationLatitude() {
        return locationLatitude;
    }

    public void setLocationLatitude(Double locationLatitude) {
        this.locationLatitude = locationLatitude;
    }

    public Integer getRadiusMeters() {
        return radiusMeters;
    }

    public void setRadiusMeters(Integer radiusMeters) {
        this.radiusMeters = radiusMeters;
    }
}
