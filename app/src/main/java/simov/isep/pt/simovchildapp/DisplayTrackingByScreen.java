package simov.isep.pt.simovchildapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import simov.isep.pt.simovchildapp.location.GeofencingUtils;
import simov.isep.pt.simovchildapp.rest.AllowedZone;
import simov.isep.pt.simovchildapp.rest.Tracking;
import simov.isep.pt.simovchildapp.rest.Username;

public class DisplayTrackingByScreen extends AppCompatActivity {
    // Getting the last known location
    // https://developer.android.com/training/location/retrieve-current.html
    private List<Tracking> mTrackingList;
    private ListView mTrackingListView;
    View.OnClickListener fabAddParent = view -> {
        final EditText taskEditText = new EditText(DisplayTrackingByScreen.this);
        AlertDialog dialog = new AlertDialog.Builder(DisplayTrackingByScreen.this)
                .setTitle("Add a new parent")
                .setMessage("Select your parent username, in order to add the tracking.")
                .setView(taskEditText)
                .setPositiveButton("Add", (dialog1, which) -> {
                    SimovChildApplication.sSimovRestAPI.addParent(SimovChildApplication.sChildUsername,
                            new Username(taskEditText.getText().toString())).enqueue(new Callback<Void>() {

                        private AlertDialog getErrorDialog(String message) {
                            return new AlertDialog.Builder(DisplayTrackingByScreen.this)
                                    .setTitle("Failure to add parent")
                                    .setMessage(message)
                                    .setNeutralButton("OK", null)
                                    .create();
                        }

                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            if (response.isSuccessful()) {
                                Snackbar.make(view, "Parent is now tracking", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                                updateTrackings();
                            } else {
                                getErrorDialog("Parent does not exist or is already tracking").show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            getErrorDialog(t.getMessage()).show();
                        }
                    });
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();

    };

    private ListView.OnItemClickListener onItemClickListener = (parent, view, position, id) -> {
        Tracking tracking = mTrackingList.get(position);

        AllowedZone zone = tracking.getAllowedZone();
        if (zone.getLocationLatitude().equals(0.0d)) {
            return;
        }

        Uri gmmIntentUri = Uri.parse(String.format(Locale.ENGLISH, "geo:%f,%f?z=%d",
                zone.getLocationLatitude(),
                zone.getLocationLongitude(),
                19));
        Log.d("MapsIntent", gmmIntentUri.toString());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    };

    private ListView.OnItemLongClickListener onItemLongClickListener = (parent, view, position, id) -> {
        Tracking tracking = mTrackingList.get(position);

        new AlertDialog.Builder(DisplayTrackingByScreen.this)
                .setTitle("Delete")
                .setMessage("Should delete tracking by " + tracking.getParentUsername() + "?")
                .setPositiveButton("Yes", (dialog, which) -> {
                    Log.d("ASD", "Deleting tracking");
                    SimovChildApplication.sSimovRestAPI.removeTracking(
                            SimovChildApplication.sChildUsername,
                            tracking.getId()).enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            if (response.isSuccessful()) {
                                updateTrackings();
                            } else {
                                onFailure(null, null);
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            new AlertDialog.Builder(DisplayTrackingByScreen.this)
                                    .setNeutralButton("OK", null)
                                    .setTitle("Error")
                                    .setMessage("Could not delete tracking. Try again later.")
                                    .create()
                                    .show();
                        }
                    });
                })
                .setNegativeButton("Cancel", null)
                .create()
                .show();

        return true;
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_tracking_by_screen);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTrackingListView = findViewById(R.id.lv_parents_tracking);
        mTrackingListView.setOnItemLongClickListener(onItemLongClickListener);
        mTrackingListView.setOnItemClickListener(onItemClickListener);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(fabAddParent);
    }

    private void updateTrackings() {
        SimovChildApplication.sSimovRestAPI.getTrackings(SimovChildApplication.sChildUsername).enqueue(new Callback<List<Tracking>>() {
            @Override
            public void onResponse(Call<List<Tracking>> call, Response<List<Tracking>> response) {
                if (response.isSuccessful()) {
                    TrackingAdapter trackingAdapter = new TrackingAdapter(
                            getApplicationContext(),
                            R.layout.tracking_row,
                            response.body());
                    mTrackingListView.setAdapter(trackingAdapter);
                    trackingAdapter.notifyDataSetChanged();
                    mTrackingList = response.body();

                    GeofencingUtils.setupGeofences(getApplicationContext(), mTrackingList);
                }
            }

            @Override
            public void onFailure(Call<List<Tracking>> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateTrackings();
    }
}
