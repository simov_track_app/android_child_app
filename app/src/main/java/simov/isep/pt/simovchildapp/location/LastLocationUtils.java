package simov.isep.pt.simovchildapp.location;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import simov.isep.pt.simovchildapp.SimovChildApplication;
import simov.isep.pt.simovchildapp.rest.Location;

/**
 * Created by joao on 29/11/2017.
 */

public class LastLocationUtils {
    private static final String TAG = "LastLocationUtils";

    @SuppressLint("MissingPermission")
    public static void postLastLocation(Context context, String trackingId) {
        FusedLocationProviderClient mFusedLocationClient =
                LocationServices.getFusedLocationProviderClient(context);

        mFusedLocationClient.getLastLocation().addOnSuccessListener(
                location -> {
                    Log.d(TAG, "Location received");
                    if (location != null) {
                        Location locationToPass = new Location();
                        locationToPass.setLocationLatitude(location.getLatitude());
                        locationToPass.setLocationLongitude(location.getLongitude());
                        SimovChildApplication.sSimovRestAPI.updateLastLocation(
                                SimovChildApplication.sChildUsername,
                                Integer.valueOf(trackingId),
                                locationToPass).enqueue(new Callback<Void>() {
                            @Override
                            public void onResponse(Call<Void> call, Response<Void> response) {
                                if (response.isSuccessful()) {
                                    Log.d(TAG, "Successfully updated last location");
                                } else {
                                    Log.d(TAG, "Failure updating last location");
                                }
                            }

                            @Override
                            public void onFailure(Call<Void> call, Throwable t) {
                                Log.d(TAG, "Failure sending last location");
                            }
                        });
                    }
                });

    }
}
