package simov.isep.pt.simovchildapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import simov.isep.pt.simovchildapp.rest.Username;

/**
 * A login screen that offers login via username.
 */
public class LoginActivity extends AppCompatActivity {
    // UI references.
    private AutoCompleteTextView mUsernameView;
    private View mLoginFormView;
    private boolean mWasPermissionAlreadyAsked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (SimovChildApplication.sChildUsername != null) {
            Intent intent = new Intent(this, DisplayTrackingByScreen.class);
            startActivity(intent);
            finish();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mUsernameView = findViewById(R.id.childUsername);

        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (mWasPermissionAlreadyAsked) {
                finish();
            }
            mWasPermissionAlreadyAsked = true;
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    0);
            return;
        }

        Button mUsernameSignInButton = findViewById(R.id.sign_in_button);
        mUsernameSignInButton.setOnClickListener(view -> attemptLogin());
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid username, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        Log.d("Login", "Attempting login");
        // Reset errors.
        mUsernameView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();

        // Check for a valid user name.
        if (!isUsernameValid(username)) {
            mUsernameView.setError(getString(R.string.error_invalid_username));
            return;
        }

        ProgressDialog dialog = new ProgressDialog(this); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Registering. Please wait...");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        SimovChildApplication.sSimovRestAPI.createChildUser(new Username(username)).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful() || response.code() == 400) {
                    PreferenceManager.getDefaultSharedPreferences(LoginActivity.this)
                            .edit().putString("childUsername", username).apply();
                    SimovChildApplication.sChildUsername = username;
                    dialog.cancel();
                    Intent intent = new Intent(LoginActivity.this, DisplayTrackingByScreen.class);
                    startActivity(intent);
                    finish();
                } else onFailure(call, null);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("CreateUser", "Create user error: " + (t != null ? t.getMessage() : ""));
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle("Error")
                        .setMessage("Could not create user. Try a different user name or try again later.")
                        .setNeutralButton("OK", null)
                        .create()
                        .show();
                dialog.cancel();
            }
        });
    }

    private boolean isUsernameValid(String username) {
        return !username.contains(" ") && username.length() > 4;
    }
}

