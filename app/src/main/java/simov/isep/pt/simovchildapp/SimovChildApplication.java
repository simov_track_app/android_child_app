package simov.isep.pt.simovchildapp;

import android.app.Application;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import simov.isep.pt.simovchildapp.rest.FcmToken;
import simov.isep.pt.simovchildapp.rest.SimovRestAPI;

/**
 * Created by joao on 16/11/2017.
 */

public class SimovChildApplication extends Application {
    private static final String TAG = "SimovChildApp";
    public static SimovRestAPI sSimovRestAPI;
    public static String sChildUsername;

    @Override
    public void onCreate() {
        super.onCreate();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://simov-tracking.herokuapp.com/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        sSimovRestAPI = retrofit.create(SimovRestAPI.class);

        // Fetch Child Username and FCM Token
        sChildUsername = PreferenceManager.getDefaultSharedPreferences(this)
                .getString("childUsername", null);
        String fcmToken = PreferenceManager.getDefaultSharedPreferences(this)
                .getString("fcmToken", null);

        Log.d(TAG, "Child_username: " + sChildUsername);
        Log.d(TAG, "Token: " + fcmToken);

        // Update token in backend
        if (SimovChildApplication.sChildUsername != null) {
            SimovChildApplication.sSimovRestAPI.updateFcmToken(
                    SimovChildApplication.sChildUsername,
                    new FcmToken(fcmToken)
            ).enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {
                        Log.d(TAG, "Successfully updated FCM token");
                    } else {
                        Log.d(TAG, "Failed to update FCM token");
                        try {
                            Log.d(TAG, response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    t.printStackTrace();
                    Log.d(TAG, "Failed to update FCM token");
                }
            });
        }
    }
}
