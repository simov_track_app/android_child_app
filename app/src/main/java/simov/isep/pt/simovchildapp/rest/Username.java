package simov.isep.pt.simovchildapp.rest;

/**
 * Created by joao on 28/11/2017.
 */

public class Username {
    String name;

    public Username() {
    }

    public Username(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
