package simov.isep.pt.simovchildapp.location;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import simov.isep.pt.simovchildapp.rest.Tracking;

/**
 * Created by joao on 28/11/2017.
 */

public class GeofencingUtils {
    @SuppressLint("MissingPermission")
    public static void setupGeofences(Context applicationContext, List<Tracking> trackings) {
        GeofencingClient geofencingClient = LocationServices.getGeofencingClient(applicationContext);
        Log.d("GeofencingUtils", "Updating geofacing zones");
        List<Geofence> geofenceList = new ArrayList<>();

        for (Tracking t : trackings) {
            if (t.getAllowedZone().getLocationLatitude().equals(0.0d)) {
                continue;
            }
            geofenceList.add(new Geofence.Builder()
                    .setRequestId(Integer.toString(t.getId()))
                    .setNotificationResponsiveness((int) TimeUnit.SECONDS.toMillis(30L))
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_EXIT)
                    .setCircularRegion(
                            t.getAllowedZone().getLocationLatitude(),
                            t.getAllowedZone().getLocationLongitude(),
                            t.getAllowedZone().getRadiusMeters())
                    .build());
        }

        geofencingClient.removeGeofences(getGeofencePendingIntent(applicationContext));
        if (geofenceList.size() > 0) {
            Log.d("GeofencingUtils", "Adding geofences list with size " + geofenceList.size());
            geofencingClient.addGeofences(getGeofencingRequest(geofenceList),
                    getGeofencePendingIntent(applicationContext));
        }
    }

    private static PendingIntent getGeofencePendingIntent(Context context) {
        Intent intent = new Intent(context, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        PendingIntent geofencePendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return geofencePendingIntent;
    }

    private static GeofencingRequest getGeofencingRequest(List<Geofence> geofenceList) {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER | GeofencingRequest.INITIAL_TRIGGER_EXIT);
        builder.addGeofences(geofenceList);
        return builder.build();
    }
}
