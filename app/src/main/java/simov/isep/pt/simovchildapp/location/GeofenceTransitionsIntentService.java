package simov.isep.pt.simovchildapp.location;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import simov.isep.pt.simovchildapp.SimovChildApplication;
import simov.isep.pt.simovchildapp.firebase.NotificationUtils;

/**
 * Created by joao on 19/11/2017.
 */

public class GeofenceTransitionsIntentService extends IntentService {
    private static final String TAG = "GeofenceTransitions";

    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }

    public GeofenceTransitionsIntentService(String name) {
        super(name);
    }

    /**
     * Triggered when there is a transition in a geofence.
     *
     * @param intent
     */
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        Log.d(TAG, "Detected Transition");

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();
        NotificationUtils.sendNotification(this,
                "Geofence Transition",
                "Geofence transition detected: " +
                        (geofencingEvent.getGeofenceTransition() == Geofence.GEOFENCE_TRANSITION_ENTER ?
                                "Enter" : "Exit"));

        List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();
        for (Geofence g : triggeringGeofences) {
            SimovChildApplication.sSimovRestAPI.addChildrenOutOfZone(
                    SimovChildApplication.sChildUsername,
                    Integer.parseInt(g.getRequestId()),
                    geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT).enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {
                        Log.d(TAG, "Transition successfully sent to server");
                    } else {
                        Log.d(TAG, "Failed to send transition to token: " + response.toString());
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Log.d(TAG, "Failed to send transition to token");
                    t.printStackTrace();
                }
            });
        }
    }


}
