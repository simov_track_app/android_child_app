package simov.isep.pt.simovchildapp.firebase;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import simov.isep.pt.simovchildapp.location.LastLocationUtils;

/**
 * Created by joao on 16/11/2017.
 */

public class FirebaseListenerService extends FirebaseMessagingService {
    private static final String TAG = "FirebaseListenerService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d(TAG, "Message received!");
        if (remoteMessage.getData() != null && remoteMessage.getData().containsKey("last-location")) {
            String trackingId = remoteMessage.getData().get("trackingId");
            Log.d(TAG, "Send last location to tracking with ID: " + trackingId);
            LastLocationUtils.postLastLocation(getApplicationContext(), trackingId);
        } else {
            NotificationUtils.sendNotification(
                    this,
                    remoteMessage.getNotification().getTitle(),
                    remoteMessage.getNotification().getBody());
        }
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }
}
