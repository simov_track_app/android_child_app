package simov.isep.pt.simovchildapp.rest;

/**
 * Created by jmiguelc on 28/12/2017.
 */

public class Location {
    private double locationLatitude;
    private double locationLongitude;

    public double getLocationLatitude() {
        return locationLatitude;
    }

    public void setLocationLatitude(double locationLatitude) {
        this.locationLatitude = locationLatitude;
    }

    public double getLocationLongitude() {
        return locationLongitude;
    }

    public void setLocationLongitude(double locationLongitude) {
        this.locationLongitude = locationLongitude;
    }
}
