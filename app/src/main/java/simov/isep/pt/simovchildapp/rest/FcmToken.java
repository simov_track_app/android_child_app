package simov.isep.pt.simovchildapp.rest;

/**
 * Created by joao on 28/11/2017.
 */

public class FcmToken {
    private String fcmToken;

    public FcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public FcmToken() {

    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
