package simov.isep.pt.simovchildapp;
import junit.framework.Assert;

import org.junit.Test;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import simov.isep.pt.simovchildapp.rest.SimovRestAPI;
import simov.isep.pt.simovchildapp.rest.Tracking;
import simov.isep.pt.simovchildapp.rest.Username;

/**
 * Created by Moreira on 08/01/2018.
 */

public class SimovRestAPITests {

    @Test
    public void getTrackingsTest() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://simov-tracking.herokuapp.com/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        SimovRestAPI  sSimovRestAPI = retrofit.create(SimovRestAPI.class);

        sSimovRestAPI.getTrackings("child_user").enqueue(new Callback<List<Tracking>>() {
            @Override
            public void onResponse(Call<List<Tracking>> call, Response<List<Tracking>> response) {
                if (response.isSuccessful() || response.code() == 400) {
                    Assert.assertTrue(true);
                } else onFailure(call, null);
            }

            @Override
            public void onFailure(Call<List<Tracking>> call, Throwable t) {
                Assert.fail();
            }
        });

    }

    @Test
    public  void createChildUserTest(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://simov-tracking.herokuapp.com/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        SimovRestAPI  sSimovRestAPI = retrofit.create(SimovRestAPI.class);
        sSimovRestAPI.createChildUser(new Username("new_child_user")).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Assert.assertTrue(true);
                } else onFailure(call, null);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Assert.fail();
            }
        });

    }


    @Test
    public  void addParentTest(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://simov-tracking.herokuapp.com/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        SimovRestAPI  sSimovRestAPI = retrofit.create(SimovRestAPI.class);
        sSimovRestAPI.addParent("new_child_user",new Username("parent_user")).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Assert.assertTrue(true);
                } else onFailure(call, null);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Assert.fail();
            }
        });

    }

}
