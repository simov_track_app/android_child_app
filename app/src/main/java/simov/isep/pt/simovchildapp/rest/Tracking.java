package simov.isep.pt.simovchildapp.rest;

/**
 * Created by joao on 28/11/2017.
 */

public class Tracking {
    Integer id;
    String parentUsername;
    String childUsername;
    Double lastLocationLatitude;
    Double lastLocationLongitude;
    AllowedZone allowedZone;

    public Tracking() {

    }

    public AllowedZone getAllowedZone() {
        return allowedZone;
    }

    public void setAllowedZone(AllowedZone allowedZone) {
        this.allowedZone = allowedZone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParentUsername() {
        return parentUsername;
    }

    public void setParentUsername(String parentUsername) {
        this.parentUsername = parentUsername;
    }

    public String getChildUsername() {
        return childUsername;
    }

    public void setChildUsername(String childUsername) {
        this.childUsername = childUsername;
    }

    public Double getLastLocationLatitude() {
        return lastLocationLatitude;
    }

    public void setLastLocationLatitude(Double lastLocationLatitude) {
        this.lastLocationLatitude = lastLocationLatitude;
    }

    public Double getLastLocationLongitude() {
        return lastLocationLongitude;
    }

    public void setLastLocationLongitude(Double lastLocationLongitude) {
        this.lastLocationLongitude = lastLocationLongitude;
    }
}
