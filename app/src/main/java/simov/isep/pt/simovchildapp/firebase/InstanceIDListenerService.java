package simov.isep.pt.simovchildapp.firebase;

import android.preference.PreferenceManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import simov.isep.pt.simovchildapp.SimovChildApplication;
import simov.isep.pt.simovchildapp.rest.FcmToken;

/**
 * Created by joao on 16/11/2017.
 */

public class InstanceIDListenerService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("InstanceIDListener", "Refreshed token: " + refreshedToken);

        PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putString("fcmToken", refreshedToken).apply();
        if (SimovChildApplication.sChildUsername != null) {
            SimovChildApplication.sSimovRestAPI.updateFcmToken(
                    SimovChildApplication.sChildUsername,
                    new FcmToken(refreshedToken)
            ).enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {

                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {

                }
            });
        }
    }
}
