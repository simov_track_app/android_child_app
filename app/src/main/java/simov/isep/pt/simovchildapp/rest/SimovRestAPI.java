package simov.isep.pt.simovchildapp.rest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by joao on 28/11/2017.
 */

public interface SimovRestAPI {
    @GET("/children/{child_user}/trackByParent")
    Call<List<Tracking>> getTrackings(@Path("child_user") String childUsername);

    @POST("/children")
    Call<Void> createChildUser(@Body Username username);

    @PUT("/children/{child_user}/token")
    Call<Void> updateFcmToken(@Path("child_user") String childUsername, @Body FcmToken fcmToken);

    @POST("/children/{child_user}/trackByParent")
    Call<Void> addParent(@Path("child_user") String childUsername, @Body Username parentName);

    @POST("/children/{child_user}/zoneTransition/{tracking_id}/out")
    Call<Void> addChildrenOutOfZone(@Path("child_user") String childUsername,
                                    @Path("tracking_id") Integer trackingId,
                                    @Query("out") boolean out);

    @POST("/children/{child_user}/last-location/{tracking_id}")
    Call<Void> updateLastLocation(@Path("child_user") String childUsername,
                                  @Path("tracking_id") Integer trackingId,
                                  @Body Location out);

    @DELETE("/children/{child_user}/tracking/{tracking_id}")
    Call<Void> removeTracking(@Path("child_user") String childUsername, @Path("tracking_id") Integer trackingId);
}
